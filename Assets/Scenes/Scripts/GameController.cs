﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{

	private CubePos nowCube = new CubePos(0, 1, 0);
	public float cubeChangePlaceSpeed = 0.5f;
	public Transform cubeToPlace;
	public GameObject allCubes, vfx;
	public GameObject[] canvasStartPage, cubesToCreate;
	public Text scoreTxt;
	private Rigidbody allCubesRb;
	private bool IsLoose, firstCube;
	private float camMoveToYPosition, camMoveSpeed = 2f;
	private int prevMaxHorizontal;
	public Color[] bgColors;
	private Color toCameraColor;
	private List<Vector3> allCubesPositions = new List<Vector3> {
		new Vector3(0, 0, 0),
		new Vector3(1, 0, 0),
		new Vector3(-1, 0, 0),
		new Vector3(0, 1, 0),
		new Vector3(0, 0, 1),
		new Vector3(0, 0, -1),
		new Vector3(1, 0, 1),
		new Vector3(-1, 0, -1),
		new Vector3(-1, 0, 1),
		new Vector3(1, 0, -1),
	};

	private Coroutine showCubePlace;
	private Transform mainCam;
	private List<GameObject> possibleCubesToCreate = new List<GameObject>();
	private void Start()
	{
        if (PlayerPrefs.GetInt("score") < 5)
        {
			possibleCubesToCreate.Add(cubesToCreate[0]);
        }
		else if (PlayerPrefs.GetInt("score") < 10)
		{
			AddPosibleCubes(2);
		}
		else if (PlayerPrefs.GetInt("score") < 15)
		{
			AddPosibleCubes(3);
		}
		else if (PlayerPrefs.GetInt("score") < 20)
		{
			AddPosibleCubes(4);
		}
		else if (PlayerPrefs.GetInt("score") < 50)
		{
			AddPosibleCubes(5);
		}
		else if (PlayerPrefs.GetInt("score") < 70)
		{
			AddPosibleCubes(6);
		}
		else if (PlayerPrefs.GetInt("score") < 90)
		{
			AddPosibleCubes(7);
		}
		else if (PlayerPrefs.GetInt("score") < 100)
		{
			AddPosibleCubes(8);
		}
		else if (PlayerPrefs.GetInt("score") < 130)
		{
			AddPosibleCubes(9);
		}
		else
		{
			AddPosibleCubes(10);
		}

		scoreTxt.text = $"<size=45><color=#F31313>BEST</color>:</size> {PlayerPrefs.GetInt("score")}\n <size=35> NOW:</size> 0";
		toCameraColor = Camera.main.backgroundColor;
		mainCam = Camera.main.transform;
		camMoveToYPosition = 5.9f + nowCube.y - 1f;
		allCubesRb = allCubes.GetComponent<Rigidbody>();
		showCubePlace = StartCoroutine(ShowCubePlace());
	}

	private void Update()
	{
		if ((Input.GetMouseButtonDown(0) || Input.touchCount > 0) && cubeToPlace != null && allCubes != null &&  !EventSystem.current.IsPointerOverGameObject())
		{
#if !UNITY_EDITOR
			if (Input.GETTOUCH(0).phase != TouchPhase.Began) return;
#endif
			if (!firstCube) {
				foreach (GameObject obj in canvasStartPage)
					Destroy(obj);
				firstCube = true; 
			}

			GameObject createCube = null;
			if (possibleCubesToCreate.Count == 1)
				createCube = possibleCubesToCreate[0];
			else
				createCube = possibleCubesToCreate[UnityEngine.Random.Range(0, possibleCubesToCreate.Count)];

			GameObject newCube = Instantiate(
				createCube, 
				cubeToPlace.position, Quaternion.identity) as GameObject;
			newCube.transform.SetParent(allCubes.transform);
			nowCube.setVector(cubeToPlace.position);
			allCubesPositions.Add(nowCube.getVector());
			if (PlayerPrefs.GetString("music") == "No")
			{
				GetComponent<AudioSource>().Play();
			}
			GameObject newVfx = Instantiate(vfx, cubeToPlace.position, Quaternion.identity) as GameObject;
			Destroy(newVfx, 1.5f);
			GetComponent<AudioSource>().Play();
			allCubesRb.isKinematic = true;
			allCubesRb.isKinematic = false;
			SpawnPosition();
			MoveCameraChangeBg();
		}

		if(!IsLoose && allCubesRb.velocity.magnitude > 0.1f)
        {
			Destroy(cubeToPlace.gameObject);
			IsLoose = true;
			StopCoroutine(showCubePlace);
		}

		mainCam.localPosition = Vector3.MoveTowards(mainCam.localPosition, new Vector3(mainCam.localPosition.x, camMoveToYPosition, mainCam.localPosition.z), camMoveSpeed * Time.deltaTime);
			
		if(Camera.main.backgroundColor != toCameraColor)
        {
			Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, toCameraColor, Time.deltaTime / 1f);
        }
	}

	IEnumerator ShowCubePlace()
	{
		while (true)
		{
			SpawnPosition();
			yield return new WaitForSeconds(cubeChangePlaceSpeed);
		}
	}

	private void SpawnPosition()
	{
		List<Vector3> positions = new List<Vector3>();
		if (IsPositionEmpty(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z)) && nowCube.x + 1 != cubeToPlace.position.x)
			positions.Add(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z));
		if (IsPositionEmpty(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z)) && nowCube.x - 1 != cubeToPlace.position.x)
			positions.Add(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z));
		if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z)) && nowCube.y + 1 != cubeToPlace.position.y)
			positions.Add(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z));
		if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z)) && nowCube.y - 1 != cubeToPlace.position.y)
			positions.Add(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z));
		if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1)) && nowCube.z + 1 != cubeToPlace.position.z)
			positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1));
		if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1)) && nowCube.z - 1 != cubeToPlace.position.z)
			positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1));
		if (positions.Count > 1)
			cubeToPlace.position = positions[UnityEngine.Random.Range(0, positions.Count)];
		else if (positions.Count == 0)
			IsLoose = true;
		else
			cubeToPlace.position = positions[0];
	}

	private bool IsPositionEmpty(Vector3 targetPos)
	{
		if (targetPos.y == 0)
			return false;
		foreach (Vector3 pos in allCubesPositions)
			if (pos.x == targetPos.x && pos.y == targetPos.y && pos.z == targetPos.z)
				return false;
		return true;
	}

	private void MoveCameraChangeBg()
    {
		int maxX = 0, maxY = 0, maxZ = 0, maxHor;

		foreach(Vector3 pos in allCubesPositions)
        {
			if (Mathf.Abs(Convert.ToInt32(pos.x)) > maxX)
				maxX = Convert.ToInt32(pos.x);
			if (Mathf.Abs(Convert.ToInt32(pos.y)) > maxY) 
				maxY = Convert.ToInt32(pos.y);
			if (Mathf.Abs(Convert.ToInt32(pos.z)) > maxZ) 
				maxZ = Convert.ToInt32(pos.z);

		}

		if (PlayerPrefs.GetInt("score") < maxY - 1)
			PlayerPrefs.SetInt("score", maxY - 1);
		scoreTxt.text = $"<size=45><color=#F31313>BEST</color>:</size> {PlayerPrefs.GetInt("score")}\n <size=35> NOW:</size> {maxY - 1}";
		camMoveToYPosition = 5.9f + nowCube.y - 1f;
		maxHor = Math.Max(maxX, maxZ);

		if (maxHor % 3 == 0 && maxHor != prevMaxHorizontal)
        {
			mainCam.localPosition -= new Vector3(0, 0, 2.5f);
			prevMaxHorizontal = maxHor;
        }

		if (maxY >= 7)
			toCameraColor = bgColors[2];
		else if (maxY >= 5)
			toCameraColor = bgColors[1];
		else if (maxY >= 2)
			toCameraColor = bgColors[0];
	}

	private void AddPosibleCubes(int till)
    {
		for (int i = 0; i < till; i++)
			possibleCubesToCreate.Add(cubesToCreate[i]);
    }
}

struct CubePos
{
	public int x, y, z;

	public CubePos(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3 getVector()
	{
		return new Vector3(x, y, z);
	}

	public void setVector(Vector3 pos)
	{
		x = Convert.ToInt32(pos.x);
		y = Convert.ToInt32(pos.y);
		z = Convert.ToInt32(pos.z);
	}
}

